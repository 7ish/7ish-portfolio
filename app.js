const overlay = document.querySelector('.overlay');
const navigation = document.querySelector('header');
const navItems = document.querySelectorAll('header nav a');
const heroHeadings = document.querySelectorAll('.hero-heading');
const galleryBtns = document.querySelectorAll('.hero-button');
const galleryOverlay = document.querySelector('#galleryOverlay');
const logo = document.querySelector('.logo');

//MAIN NAVIGATION

for (let item of navItems) {
    item.addEventListener('mouseover', () => {
        overlay.classList.add('active');
        let position = item.getBoundingClientRect();
        overlay.style.left = position.x + 'px';
        overlay.style.top = position.y + 'px';
        overlay.style.height = position.height + 'px';
        overlay.style.width = position.width + 'px';
    })
    item.addEventListener('mouseout', () => {
        overlay.classList.remove('active');
    })
}


// ANIMATIONS

    window.onscroll = function () {
        animateLogo();
    };

    function animateLogo() {
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            logo.classList.add('logo-small');
            navigation.classList.remove('animation-away');
        } else {
            logo.classList.remove('logo-small');
            navigation.classList.add('animation-away');

        }
    }





const observer = new IntersectionObserver(entries => {
    for (let entry of entries) {
        const button = entry.target.querySelector('.hero-button');
        const header = entry.target.previousElementSibling;

        if (entry.isIntersecting) {
            button.classList.add('animation-button');
            header.classList.add('animation-heading');
            return;
        }
        button.classList.remove('animation-button');
        header.classList.remove('animation-heading');

    }
})

// observer.observe(document.querySelector('#foodGalleryHolder'));

const holders = document.querySelectorAll('.hero-button-holder');
for (const holder of holders) {
    observer.observe(holder);
}


