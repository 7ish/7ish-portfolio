const imageCarousel = document.querySelector('.tumbnail');
const placesImages = [];


for (let i = 1; i <= 10; i++) {
    placesImages.push(`assets/images/places/places${i}.jpg`);
}

for (image of placesImages) {
    const img = document.createElement('img');
    img.setAttribute('src', image);
    document.querySelector('.tumbnail').append(img);
    console.log(img)
    document.querySelector('#galleryMain').innerHTML += 
    `<section>
        <img src="${image}" class="hero-image-full">
    </section>
    `
}


document.querySelectorAll('.tumbnail img').forEach(li => {
    li.addEventListener('click', e => {
      e.currentTarget.classList.add('loading')
    })
    
    li.addEventListener('mousemove', e => {
      let item = e.target
      let itemRect = item.getBoundingClientRect()
      let offset = Math.abs(e.clientX - itemRect.left) / itemRect.width
      
      let prev = item.previousElementSibling || null
      let next = item.nextElementSibling || null
      
      let scale = 0.6
      
      resetScale()
      
      if (prev) {
        prev.style.setProperty('--scale', 1 + scale * Math.abs(offset - 1))
      }
      
      item.style.setProperty('--scale', 1 + scale)
      
      if (next) {
        next.style.setProperty('--scale', 1 + scale * offset)
      }
    })
  })
  
  document.querySelector('.tumbnail').addEventListener('mouseleave', e => {
    resetScale()
  })
  
  function resetScale() {
    document.querySelectorAll('.tumbnail img').forEach(li => {
      li.style.setProperty('--scale', 1)
    })
  }